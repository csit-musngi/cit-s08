A. 
SELECT * from artists where name LIKE '%D' OR name LIKE '%D%' OR name LIKE '%D';

B.
SELECT * from songs where length < 230;

C.
SELECT album_title, song_name, length FROM songs JOIN albums on songs.album_id = albums.id;

D.
SELECT * from artists JOIN albums on artists.id = albums.artist_id where name LIKE '%A' OR name LIKE '%A%' OR name LIKE '%A';

E. 
SELECT * from albums ORDER BY album_title  LIMIT 4;


F.
SELECT * FROM albums JOIN songs on songs.album_id = albums.id ORDER BY album_title DESC, song_name ASC;
